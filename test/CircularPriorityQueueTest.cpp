#include <gtest/gtest.h>
#include "../src/CircularPriorityQueue.hpp"

TEST(CircularPriorityQueueTests, IsEmpty) {
    CircularPriorityQueue<int> queue(5);

    EXPECT_TRUE(queue.isEmpty());
}

TEST(CircularPriorityQueueTests, IsFull) {
    CircularPriorityQueue<int> queue(1);

    queue.enqueue(1, 1);

    EXPECT_TRUE(queue.isFull());
}

TEST(CircularPriorityQueueTests, AdvanceOneElementForward_Trivial) {
    CircularPriorityQueue<int> queue(5);

    size_t idx = 0;
    queue.advanceOneElementForward(idx);
    EXPECT_EQ(1, idx);
}

TEST(CircularPriorityQueueTests, AdvanceOneElementForward_AtRightArrayBound) {
    CircularPriorityQueue<int> queue(5);

    size_t idx = 4;
    queue.advanceOneElementForward(idx);
    EXPECT_EQ(0, idx);
}

TEST(CircularPriorityQueueTests, AdvanceOneElementBackward_Trivial) {
    CircularPriorityQueue<int> queue(5);

    size_t idx = 1;
    queue.advanceOneElementBackward(idx);
    EXPECT_EQ(0, idx);
}

TEST(CircularPriorityQueueTests, AdvanceOneElementBackward_AtLeftArrayBound) {
    CircularPriorityQueue<int> queue(5);

    size_t idx = 0;
    queue.advanceOneElementBackward(idx);
    EXPECT_EQ(4, idx);
}


TEST(CircularPriorityQueueTests, GetDistanceFromHeadToIndex_Straight) {
    CircularPriorityQueue<int> queue(5);

    queue.head = 0;
    queue.tail = 4;

    EXPECT_EQ(0, queue.getDistanceFromHeadTo(0));
    EXPECT_EQ(1, queue.getDistanceFromHeadTo(1));
    EXPECT_EQ(4, queue.getDistanceFromHeadTo(4));
}

TEST(CircularPriorityQueueTests, GetDistanceFromHeadToIndex_Bending) {
    CircularPriorityQueue<int> queue(5);

    queue.head = 2;
    queue.tail = 1;

    EXPECT_EQ(2, queue.getDistanceFromHeadTo(4));
    EXPECT_EQ(3, queue.getDistanceFromHeadTo(0));
}

TEST(CircularPriorityQueueTests, GetDistanceFromIndexToTail_Straight) {
    CircularPriorityQueue<int> queue(5);

    queue.head = 0;
    queue.tail = 4;

    EXPECT_EQ(3, queue.getDistanceToTailFrom(1));
    EXPECT_EQ(0, queue.getDistanceToTailFrom(4));
}

TEST(CircularPriorityQueueTests, GetDistanceFromIndexToTail_Bending) {
    CircularPriorityQueue<int> queue(5);

    queue.head = 2;
    queue.tail = 1;

    EXPECT_EQ(3, queue.getDistanceToTailFrom(3));
    EXPECT_EQ(1, queue.getDistanceToTailFrom(0));
}


class CircularPriorityQueueTests_FindInsertionPlace_Fixture : public ::testing::Test {
protected:
    CircularPriorityQueue<int> *queue;

    virtual void SetUp() {
        queue = new CircularPriorityQueue<int>(10);

        queue->head = 5;

        queue->priorities[5] = 15;
        queue->priorities[6] = 13;
        queue->priorities[7] = 11;
        queue->priorities[8] = 9;
        queue->priorities[9] = 7;
        queue->priorities[0] = 5;
        queue->priorities[1] = 3;
        queue->priorities[2] = 1;

        queue->tail = 3;
    }

    virtual void TearDown() {
        delete queue;
    }
};

TEST_F(CircularPriorityQueueTests_FindInsertionPlace_Fixture, FindInsertionPlaceForGivenPriority) {
    EXPECT_EQ(7, queue->findInsertionPlaceFor(12));
    EXPECT_EQ(0, queue->findInsertionPlaceFor(6));
    EXPECT_EQ(2, queue->findInsertionPlaceFor(2));
    EXPECT_EQ(5, queue->findInsertionPlaceFor(15));
    EXPECT_EQ(9, queue->findInsertionPlaceFor(8));
    EXPECT_EQ(5, queue->findInsertionPlaceFor(20));
}


TEST_F(CircularPriorityQueueTests_FindInsertionPlace_Fixture, GetItemsQuantity) {
    EXPECT_EQ(8, queue->getItemsQuantity());
}


TEST(CircularPriorityQueueTests, GetItemsQuantity_FromEmptyQueue) {
    CircularPriorityQueue<int> queue(5);
    
    EXPECT_EQ(0, queue.getItemsQuantity());
}


class CircularPriorityQueueTests_EnqueueAndDequeue_Fixture : public ::testing::Test {
private:
    const int MIN_PRIORITY = 1;
    const int MAX_PRIORITY = 20;

    const size_t QUEUE_SIZE = 50;

protected:
    CircularPriorityQueue<int> *queue;

    virtual void SetUp() {
        queue = new CircularPriorityQueue<int>(QUEUE_SIZE);

        // init random generator
        srand(time(NULL));
    }

    virtual void TearDown() {
        delete queue;
    }

    int getRandomInteger() {
        return std::rand() % MAX_PRIORITY + MIN_PRIORITY;
    }

    bool isPrioritiesSorted() {
        size_t latest = queue->tail;
        queue->advanceOneElementBackward(latest);

        for (size_t i = queue->head; i != latest; queue->advanceOneElementForward(i)) {
            size_t j = i;
            queue->advanceOneElementForward(j);

            if (queue->priorities[i] < queue->priorities[j])
                return false;
        }

        return true;
    }
};

TEST_F(CircularPriorityQueueTests_EnqueueAndDequeue_Fixture, EnqueueAndDequeue) {

    for (size_t i = 0; i < queue->length; ++i) {
        int randInt = getRandomInteger();
        queue->enqueue(i, randInt);
        EXPECT_TRUE(isPrioritiesSorted());
    }

    EXPECT_TRUE(queue->isFull());
    queue->print();



    size_t itemsQuantity = queue->getItemsQuantity();
    for (size_t i = 0; i < itemsQuantity; ++i) {
        int elementWithHighestPriority = queue->peek();
        EXPECT_EQ(elementWithHighestPriority, queue->dequeue());
    }

    EXPECT_TRUE(queue->isEmpty());
    queue->print();
}

TEST(CircularPriorityQueueTests, EnqueueElementWithZeroPriority) {
    CircularPriorityQueue<int> queue(5);

    EXPECT_THROW(queue.enqueue(1, 0), std::invalid_argument);
}


TEST(CircularPriorityQueueTests, EnqueueElementToFullQueue) {
    CircularPriorityQueue<int> queue(1);

    queue.enqueue(1, 1);

    EXPECT_THROW(queue.enqueue(2, 1), std::overflow_error);
}

TEST(CircularPriorityQueueTests, PeekFromEmptyQueue) {
    CircularPriorityQueue<int> queue(5);

    EXPECT_THROW(queue.peek(), std::underflow_error);
}


TEST(CircularPriorityQueueTests, ConstructQueueWithZeroLength) {
    EXPECT_THROW(new CircularPriorityQueue<int>(0), std::invalid_argument);
}

TEST(CircularPriorityQueueTests, EqualityOfTwoQueues) {
    CircularPriorityQueue<int> q1(5);

    for (size_t i = 1; i <= 3; ++i)
        q1.enqueue(i, i);


    CircularPriorityQueue<int> q2(4);

    for (size_t i = 1; i <= 3; ++i)
        q2.enqueue(i, i*2);


    EXPECT_TRUE(q1 == q2);
}

TEST(CircularPriorityQueueTests, UnequalityOfTwoQueues) {
    CircularPriorityQueue<int> q1(5);

    for (size_t i = 1; i <= 3; ++i)
        q1.enqueue(i, i);


    CircularPriorityQueue<int> q2(4);

    for (size_t i = 1; i <= 3; ++i)
        q2.enqueue(i*2, i);


    EXPECT_FALSE(q1 == q2);
}




TEST_F(CircularPriorityQueueTests_FindInsertionPlace_Fixture, AssignOperator) {
    CircularPriorityQueue<int> q1(8);
    q1 = *queue;
    q1.print();

    CircularPriorityQueue<int> q2(7);
//    EXPECT_THROW(q2 = *queue, std::length_error);
}
