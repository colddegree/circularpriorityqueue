project(CircularPriorityQueueTests)

add_subdirectory(lib/googletest)
include_directories(lib/googletest/googletest/include)

set(TEST_SOURCE_FILES CircularPriorityQueueTest.cpp)

add_executable(runTests ${TEST_SOURCE_FILES})

target_link_libraries(runTests gtest gtest_main core)