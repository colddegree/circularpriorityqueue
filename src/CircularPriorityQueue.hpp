#ifndef CIRCULAR_PRIORITY_QUEUE_HPP
#define CIRCULAR_PRIORITY_QUEUE_HPP

#include <cstddef>
#include <ostream>

// for testing purposes
//#define private public


template <typename T>
class CircularPriorityQueue {
private:
    T *entries;
    unsigned int *priorities;
    size_t length;
    size_t head, tail;
public:
    explicit CircularPriorityQueue(size_t length) noexcept(false);

    CircularPriorityQueue(const CircularPriorityQueue &queue);

    ~CircularPriorityQueue();

    void enqueue(const T &element, unsigned int priority) noexcept(false);
    T& dequeue() noexcept(false);
    T& peek() const noexcept(false);

    size_t getItemsQuantity() const;

    CircularPriorityQueue &operator=(const CircularPriorityQueue &rhs);

    bool operator==(const CircularPriorityQueue &rhs);

    void print() const;

private:
    bool isFull() const;
    bool isEmpty() const;

    size_t findInsertionPlaceFor(unsigned int priority) const;

    // TODO: rewrite these methods
    void advanceOneElementForward(size_t &index) const;
    void advanceOneElementBackward(size_t &index) const;

    size_t getDistanceFromHeadTo(size_t index) const;
    size_t getDistanceToTailFrom(size_t index) const;
};


// template functions implementation
#include "CircularPriorityQueue.t.hpp"

#endif // CIRCULAR_PRIORITY_QUEUE_HPP
