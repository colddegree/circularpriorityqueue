#ifndef CIRCULAR_PRIORITY_QUEUE_T_HPP
#define CIRCULAR_PRIORITY_QUEUE_T_HPP

#include "../lib/cpp-text-table/TextTable.h"

template <typename T>
T *allocateMemoryForEntries(size_t length) {
    return new T[length];
}

unsigned int *allocateMemoryForPriorities(size_t length) {
    return new unsigned int[length];
}

template <typename T>
CircularPriorityQueue<T>::CircularPriorityQueue(size_t length) noexcept(false) {
    if (length == 0)
        throw std::invalid_argument("Length must be greater than zero.");

    this->length = length;

    entries = allocateMemoryForEntries<T>(length);
    priorities = allocateMemoryForPriorities(length);

    for (size_t i = 0; i < length; ++i)
        priorities[i] = 0;

    head = 0;
    tail = 0;
}

template <typename T>
CircularPriorityQueue<T>::CircularPriorityQueue(const CircularPriorityQueue &queue) {
    length = queue.length;

    entries = allocateMemoryForEntries<T>(length);
    priorities = allocateMemoryForPriorities(length);

    for (size_t i = 0; i < length; ++i) {
        entries[i] = queue.entries[i];
        priorities[i] = queue.priorities[i];
    }

    head = queue.head;
    tail = queue.tail;
}

template <typename T>
CircularPriorityQueue<T>::~CircularPriorityQueue() {
    delete [] entries;
    delete [] priorities;
}

template <typename T>
void CircularPriorityQueue<T>::enqueue(const T &element, unsigned int priority) noexcept(false) {
    if ( isFull() )
        throw std::overflow_error("Queue is full.");

    if (priority == 0)
        throw std::invalid_argument("Priority must be greater than zero.");


    size_t idx = findInsertionPlaceFor(priority);

    size_t headToIdxDist = getDistanceFromHeadTo(idx);
    size_t idxToTailDist = getDistanceToTailFrom(idx);

    // shifting elements...
    if (idxToTailDist <= headToIdxDist) {

        for (size_t i = tail; i != idx; advanceOneElementBackward(i)) {
            size_t j = i;
            advanceOneElementBackward(j);
            entries[i] = entries[j];
            priorities[i] = priorities[j];
        }
        advanceOneElementForward(tail);

    } else {
        advanceOneElementBackward(head);
        advanceOneElementBackward(idx);

        for (size_t i = head; i != idx; advanceOneElementForward(i)) {
            size_t j = i;
            advanceOneElementForward(j);
            entries[i] = entries[j];
            priorities[i] = priorities[j];
        }
    }

    entries[idx] = element;
    priorities[idx] = priority;
}

template <typename T>
bool CircularPriorityQueue<T>::isFull() const {
    return priorities[tail] != 0;
}

template <typename T>
void CircularPriorityQueue<T>::advanceOneElementForward(size_t &index) const {
    if (index == length - 1)
        index = 0;
    else
        ++index;
}

template <typename T>
void CircularPriorityQueue<T>::advanceOneElementBackward(size_t &index) const {
    if (index == 0)
        index = length - 1;
    else
        --index;
}

template <typename T>
size_t CircularPriorityQueue<T>::findInsertionPlaceFor(unsigned int priority) const {

    size_t left = head;
    size_t right = tail - 1;

    if (head > tail)
        right = length + tail - 1;

    while (left < right) {
        size_t mid = (left + right) / 2;
        size_t curr = mid % length;

        if (priority < priorities[curr])
            left = mid + 1;
        else if (priority > priorities[curr])
            right = mid - 1;
        else
            return curr;
    }

    size_t placementIndex = left % length;

    if (priority < priorities[placementIndex])
        advanceOneElementForward(placementIndex);

    return placementIndex;
}

template <typename T>
size_t CircularPriorityQueue<T>::getDistanceFromHeadTo(size_t index) const {

    return index < head
           ? length - head + index
           : index - head;
}

template <typename T>
size_t CircularPriorityQueue<T>::getDistanceToTailFrom(size_t index) const {

    return index <= tail
           ? tail - index
           : length - index + tail;
}

template <typename T>
T& CircularPriorityQueue<T>::dequeue() noexcept(false) {

    T &result = peek();

    priorities[head] = 0;
    advanceOneElementForward(head);

    return result;
}

template <typename T>
T& CircularPriorityQueue<T>::peek() const noexcept(false) {
    if ( isEmpty() )
        throw std::underflow_error("Queue is empty.");

    return entries[head];
}

template <typename T>
bool CircularPriorityQueue<T>::isEmpty() const {
    return priorities[head] == 0;
}

template <typename T>
size_t CircularPriorityQueue<T>::getItemsQuantity() const {
    if ( isEmpty() )
        return 0;

    if ( isFull() )
        return length;

    return getDistanceFromHeadTo(tail);
}

template <typename T>
CircularPriorityQueue<T>& CircularPriorityQueue<T>::operator=(const CircularPriorityQueue &rhs) {
    head = 0;
    tail = 0;

    size_t idx = head;
    size_t rhsIdx = rhs.head;

    const size_t itemsQuantity = std::min( length, rhs.getItemsQuantity() );

    for (size_t i = 0; i < itemsQuantity; ++i) {
        entries[idx] = rhs.entries[rhsIdx];
        priorities[idx] = rhs.priorities[rhsIdx];

        advanceOneElementForward(idx);
        rhs.advanceOneElementForward(rhsIdx);
    }

    if (rhs.getItemsQuantity() < length)
        priorities[tail] = 0;

    return *this;
}

template <typename T>
bool CircularPriorityQueue<T>::operator==(const CircularPriorityQueue &rhs) {

    if (getItemsQuantity() != rhs.getItemsQuantity())
        return false;


    size_t idx = head;
    size_t rhsIdx = rhs.head;

    const size_t itemsQuantity = getItemsQuantity();

    for (size_t i = 0; i < itemsQuantity; ++i) {
        if (entries[idx] != rhs.entries[rhsIdx])
            return false;

        advanceOneElementForward(idx);
        rhs.advanceOneElementForward(rhsIdx);
    }

    return true;
}

template <typename T>
void CircularPriorityQueue<T>::print() const {
    TextTable t;

    t.add("Pointer");
    t.add("Index");
    t.add("Entry");
    t.add("Priority");
    t.endOfRow();

    for (size_t i = 0; i < length; ++i) {
        if (i == head && i == tail)
            t.add("head/tail");
        else if (i == head)
            t.add("head");
        else if (i == tail)
            t.add("tail");
        else
            t.add("");

        t.add( std::to_string(i) );
        t.add( std::to_string(entries[i]) );
        t.add( std::to_string(priorities[i]) );
        t.endOfRow();
    }

    t.setAlignment(3, TextTable::Alignment::RIGHT);

    std::cout << t << std::endl;
}

#endif // CIRCULAR_PRIORITY_QUEUE_T_HPP
